# C4 Boilerplate

Repository ini berisi:

- [Contoh-contoh script PlantUML](./examples/) untuk membuat diagram-diagram [C4 Model][1].
- File-file [BASH script](./plantuml-server/start) untuk membantu menjalankan atau menghentikan container Docker yang berisi PlantUML Server.
- File-file pendukung lingkungan VSCode terkait pengembangan C4 Model dengan PlantUML.
- File-file gambar [hasil _export_ terbaru](#diagram-diagram-terbaru) diagram C4 Model.

## Persyaratan Sistem

Berikut ini setumpuk peralatan yang dibutuhkan untuk bekerja pada repository ini:

- Linux based OS
- BASH Shell
- Git
- Docker
- Visual Studio Code
- [PlantUML Extension for VSCode][4]

## Persiapan Kerja

### 1. Clone Repository

Buka aplikasi terminal, pastikan menggunakan BASH shell, kemudian clone repository ini dengan perintah berikut:

> _:warning: Terlebih dahulu, pastikan SSH key sudah terkonfigurasi antara remote dan local._

```bash
# Clone repository
git clone git@gitlab.com:xpartacvs/c4-boilerplate.git

# kemudian masuk ke diretori hasil clone repository:
cd c4-boilerplate
```

### 2. Jalankan PlantUML Server

Eksekusi perintah berikut untuk menjalankan container Docker yang berisi PlantUML Server:

> _:warning: Terlebih dahulu, pastikan tidak ada service yang sedang menggunakan port 8080_

```bash
./plantuml-server/start
```

> _:bulb: Eksekusi perintah `./plantuml-server/stop` untuk menghentikan container docker ketika sudah tidak digunakan._

### 3. Setup PlantUML Extension

Untuk setup PlantUML Extension, buka terlebih dahulu project ini di VSCode kemudian ikuti langkah-langkah berikut:

#### 3.1 Install PlantUML Extension

Install [PlantUML Extension for VSCode][4] melalui menu `Extensions`.

#### 3.2 Konfigurasi PlantUML Extension

1. Buka user settings di VSCode dengan mengakses menu `File > Preferences > Settings`.
2. Setelah terbuka pastikan tabulasi yang sedang aktif adalah tab `User`
3. Pada kolom pencarian, ketik `plantuml`.
4. Pada hasil pencarian, temukan pengaturan `Plantuml: Server` dan isi kolom dengan nilai `http://localhost:8080`.
5. Pada hasil pencarian, temukan pula pengaturan `Plantuml: Render` dan pilih `PlantUMLServer`.

## Penggunaan PlantUML Extension

Secara umum penggunaan PlantUML Extension dapat dilakukan dengan cara:

1. Akses `command palette` di VSCode dengan menekan tombol `Ctrl+Shift+P`.
2. Ketikkan `PlantUML` pada kolom pencarian.
3. Pilih salah satu opsi yang tersedia.

### Pratinjau Diagram

Selama pengembangan C4 Model di VSCode, pratinjau diagram dapat dimunculkan dengan cara:

1. Buka file diagram (`*.puml`) yang akan dilihat pratinjaunya.
2. Akses `command palette` dengan menekan tombol `Ctrl+Shift+P`.
3. Ketikkan `PlantUML` pada kolom pencarian.
4. Pilih opsi `Preview Current Diagram`.

### Export Diagram

Untuk meng-_export_ diagram ke file gambar, dapat dilakukan dengan cara:

1. Akses `command palette` dengan menekan tombol `Ctrl+Shift+P`.
2. Ketikkan `PlantUML` pada kolom pencarian.
3. Pilih opsi `Export Workspace Diagrams`.
4. Pilih format file gambar yang dikehendaki: `svg | png | txt`.

## Diagram-diagram terbaru

Berikut ini adalah diagram-diagram C4 Model terbaru yang telah di-_export_ ke file gambar _(*.svg)_:

### Level 1 - Context Diagram

![Context Diagram][5]

### Level 2 - Container Diagram

![Container Diagram][5]

## Referensi

- [C4 Model][1]
- [PlantUML C4 Library references][2]
- [PlantUML C4 Library repository][3]
- [PlantUML Extension for Visual Studio Code][4]

[1]: https://c4model.com/
[2]: https://plantuml.com/stdlib#062f75176513a666
[3]: https://github.com/plantuml-stdlib/C4-PlantUML
[4]: https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
[5]: https://uploads.sitepoint.com/wp-content/uploads/2017/12/1498798784svg101.svg
